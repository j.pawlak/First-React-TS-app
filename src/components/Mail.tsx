import React from 'react';

function Mail() {
  return (
    <div>
      <i className='fa-solid fa-envelope'></i>
      <a href='mailto:email@email.com'>email@email.com</a>        
    </div>
  );
}

export default Mail;