import React from 'react';

function Location() {
  return (
    <div>
        <i className='fa-solid fa-location-dot'></i>
        <a href="https://carstwopolskie.fandom.com/pl/wiki/Chrz%C4%85szczy%C5%BCewoszyce">Chrząszczyrzewoszyce</a>    
    </div>
  );
}

export default Location;