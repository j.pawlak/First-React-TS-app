import React from 'react';
import Button from '@mui/material/Button';
import './Button.css';

function Buttons() {
  return (
    <>
      <Button variant="contained" className='btn'>Preview</Button>
    </>
  );
}

export default Buttons;