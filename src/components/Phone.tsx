import React from 'react';

function Phone() {
  return (
    <div>
        <i className='fas fa-phone'></i>
        <a href="tel:+536174339">+48 536 174 339</a>              
    </div>
  );
}

export default Phone;