import React from 'react';

function Link() {
  return (
    <div>
        <i className='fa-solid fa-link'></i>
        <a href="https://github.com/NacomiTagiera">GitHub - NacomiTagiera</a>    
    </div>
  );
}

export default Link;