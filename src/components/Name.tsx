import React from 'react';
import './Name.css'

function Name() {
  return (   
    <>
        <p className='name'>Jakub Pawlak</p>    
        <p className='job'>Front-end Dev</p>   
    </> 
  );
}

export default Name;